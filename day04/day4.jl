struct Assignment
  min::Int64
  max::Int64
end

struct Pair
  first::Assignment
  second::Assignment
end

function day4()
  solution1()
  solution2()
end

function solution1()
  pairs = getpairs()
  total_full_contains = 0

  for pair in pairs
    is_full_contain = ((pair.first.min >= pair.second.min && pair.first.max <= pair.second.max)
      || (pair.second.min >= pair.first.min && pair.second.max <= pair.first.max))
    if is_full_contain
      total_full_contains += 1
    end
  end

  println("Solution 1")
  println(total_full_contains)
end

function solution2()
  pairs = getpairs()
  total_overlaps = 0

  for pair in pairs
    isoverlap = ((pair.first.min >= pair.second.min && pair.first.min <= pair.second.max)
      || (pair.first.max >= pair.second.min && pair.first.max <= pair.second.max)
      || (pair.second.min >= pair.first.min && pair.second.min <= pair.first.max)
      || (pair.second.max >= pair.first.min && pair.second.max <= pair.first.max))
    if isoverlap
      total_overlaps += 1
    end
  end

  println("Solution 2")
  println(total_overlaps)
end

function getinput()::Array{String}
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

function getpairs()::Array{Pair}
  lines = getinput()

  pairs = map(line -> begin
    ranges = split(line, ",")
    assignments = map(range -> begin
      assigment = split(range, "-")
      min = parse(Int64, assigment[1])
      max = parse(Int64, assigment[2])
      return Assignment(min, max)
    end, ranges)
    return Pair(assignments[1], assignments[2])
  end, lines)

  pairs
end

day4()
