mutable struct Position
  elevation::Char
  y::Int64
  x::Int64
  notvalid::Bool
  found_goal::Bool
end

mutable struct State
  current::Position
  previous::Char
  grid
  start::Position
  goal::Position
  width::Int64
  height::Int64
  steps::Int64
  traversed::Array{Position}
  found::Array{Int64}
  max::Int64
end

function day12()
  solution1()
end

function solution1()
  lines = getinput()
  (grid, start, goal) = getgrid(lines)
  state = State(start, 'S', grid, start, goal, length(lines[1]), length(lines), 0, [], [], trunc(length(lines) * length(lines[1])))

  checkpath(state, 0)

  println("Solution 1")
  println(state.found)
  println(minimum(state.found))
end

function solution2()
  
end

function getgrid(lines)
  grid = []
  start = undef
  goal = undef

  for y in eachindex(lines)
    push!(grid, [])

    for x in eachindex(lines[y])
      position = Position(lines[y][x], y, x, false, false)
      if lines[y][x] == 'S'
        position.elevation = 'a'
        start = position
      elseif lines[y][x] == 'E'
        position.elevation = 'z'
        goal = position
      end
      push!(grid[y], position)
    end
  end

  (grid, start, goal)
end

function getnext(direction::Char, state::State)
  if direction == state.previous return nothing end
  
  next = nothing

  if direction == 'R'
    if state.current.x == state.width return nothing end
    next = state.grid[state.current.y][state.current.x + 1]
  elseif direction == 'L'
    if state.current.x == 1 return nothing end
    next = state.grid[state.current.y][state.current.x - 1]
  elseif direction == 'U'
    if state.current.y == 1 return nothing end
    next = state.grid[state.current.y - 1][state.current.x]
  elseif direction == 'D'
    if state.current.y == state.height return nothing end
    next = state.grid[state.current.y + 1][state.current.x]
  end

  if next.notvalid
    return nothing
  end

  if length(filter(x -> x == next, state.traversed)) > 0
    return nothing
  end

  isvalid = state.current == state.start || ((next != state.start) && (state.current.elevation + 1 >= next.elevation))

  return isvalid ? next : nothing
end

function checkpath(state::State, steps::Int64)
  # println(state.current.elevation * " " * string(state.current.y) * " " * string(state.current.x))
  # println(steps)
  if state.max !== nothing && steps > state.max
    false
  end
  if state.current == state.goal
    if steps < state.max
      state.max = steps
    end
    return true
  end
  steps += 1
  directions = ['R', 'D', 'L', 'U']
  current = state.current
  previous = state.previous
  push!(state.traversed, state.current)
  traversed = copy(state.traversed)

  # println(map(x -> x.elevation * " " * string(x.y) * "-" * string(x.x), state.traversed))

  for direction in directions
    state.current = current
    state.previous = previous
    next = getnext(direction, state)
    if next === nothing
      continue
    end

    state.current = next

    state.previous = getprevious(direction)
    found_goal = checkpath(state, steps)

    if found_goal
      push!(state.found, state.max)
      println(state.max)
    end
    state.traversed = traversed
  end

  state.traversed = traversed


  return false
end

function getprevious(direction::Char)
  if direction == 'R'
    return 'L'
  elseif direction == 'L'
    return 'R'
  elseif direction == 'U'
    return 'D'
  else direction == 'D'
    return 'U'
  end
end


function getinput()
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

day12()


#634