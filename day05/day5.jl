struct Rearrangement
  move::Int64
  from::Int64
  to::Int64
end

function day5()
  solution1()
  solution2()
end

function solution1()
  stacks, rearrangements = getstacks()

  for rearrangement in rearrangements
    counter = 0
    while counter < rearrangement.move
      from_stack = stacks[rearrangement.from]
      to_stack = stacks[rearrangement.to]
      insert!(to_stack, 1, from_stack[1])
      deleteat!(from_stack, 1)
      counter += 1
    end
  end

  result = join(map(stack -> stack[1], stacks))

  println("Solution 1")
  println(result)
end

function solution2()
  stacks, rearrangements = getstacks()

  for rearrangement in rearrangements
    from_stack = stacks[rearrangement.from]
    to_stack = stacks[rearrangement.to]
    crates_to_move = from_stack[1:rearrangement.move]

    stacks[rearrangement.from] = from_stack[(rearrangement.move + 1):end]
    stacks[rearrangement.to] = cat(crates_to_move, to_stack, dims=1)
  end

  result = join(map(stack -> stack[1], stacks))

  println("Solution 2")
  println(result)
end

function getinput()
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

function getstacks()
  lines = getinput()

  stacks = [[], [], [], [] ,[], [], [], [], []]
  rearrangements = []
  is_on_stacks = true

  for line in lines
    if line == ""
      is_on_stacks = false
      continue
    end

    if is_on_stacks
      addstacks!(stacks, line)
    else
      addrearrangements!(rearrangements, line)
    end
    
  end

  return stacks, rearrangements
end

function addstacks!(stacks::Vector{Vector{Any}}, line::String)
  stackindex = 1
  while stackindex <= ceil((length(line) / 4))
    crate = line[stackindex * 4 - 2]
    if isletter(crate)
      push!(stacks[stackindex], crate)
    end
    stackindex += 1
  end
end

function addrearrangements!(rearrangements::Vector{Any}, line::String)
  words = split(line, " ")
  move = parse(Int64, words[2])
  from = parse(Int64, words[4])
  to = parse(Int64, words[6])
  push!(rearrangements, Rearrangement(move, from, to))
end

day5()
