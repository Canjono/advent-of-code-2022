@enum Shape begin
  Rock = 1
  Paper = 2
  Scissors = 3
end

function day2()
  solution1()
  solution2()
end

function solution1()
  games = getgames1()
  totalscore = 0

  for game in games
    totalscore += get_game_score(game[1], game[2])
  end

  println("Solution 1")
  println(totalscore)
end

function solution2()
  games = getgames2()
  totalscore = 0

  for game in games
    totalscore += get_game_score(game[1], game[2])
  end

  println("Solution 2")
  println(totalscore)
end

function get_game_score(opponent::Shape, me::Shape)
  score = Int64(me)

  if opponent == me
    return score += 3
  end

  won = ((opponent == Rock && me == Paper)
    || (opponent == Paper && me == Scissors)
    || (opponent == Scissors && me == Rock))

  if won
    score += 6
  end

  score
end

function getinput()
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

  lines
end

function getgames1()
  lines = getinput()  

  games = []

  for line in lines
    chars = split(line, " ")
    opponentshape = getshape(chars[1])
    myshape = getshape(chars[2])
    push!(games, [opponentshape, myshape])
  end

	games
end

function getgames2()
  lines = getinput()  

  games = []

  for line in lines
    chars = split(line, " ")
    opponentshape = getshape(chars[1])
    expected_result = get_expected_result(chars[2])
    myshape = get_my_shape(opponentshape, expected_result)
    push!(games, [opponentshape, myshape])
  end

	games
end

function getshape(character::SubString{String})
  if character == "A" || character == "X"
    return Rock
  elseif character == "B" || character == "Y"
    return Paper
  elseif character == "C" || character == "Z"
    return Scissors
  end
end

function get_expected_result(character::SubString{String})
  if character == "X"
    return "lose"
  elseif character == "Y"
    return "draw"
  else
    return "win"
  end

end

function get_my_shape(opponent::Shape, expected_result::String)
  if expected_result == "win"
    return get_winning_shape(opponent)
  elseif expected_result == "lose"
    return get_losing_shape(opponent)
  else
    return opponent
  end
end

function get_winning_shape(opponent::Shape)
  if opponent == Rock
    return Paper
  elseif opponent == Paper
    return Scissors
  else
    return Rock
  end
end

function get_losing_shape(opponent::Shape)
  if opponent == Rock
    return Scissors
  elseif opponent == Paper
    return Rock
  else
    return Paper
  end
end

day2()
