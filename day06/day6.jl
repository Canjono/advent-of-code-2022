function day6()
  solution1()
  solution2()
end

function solution1()
  input = getinput()
  result = get_first_marker(input, 4)

  println("Solution 1")
  println(result)
end

function solution2()
  input = getinput()
  result = get_first_marker(input, 14)

  println("Solution 2")
  println(result)
end

function getinput()
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines[1]
end

function get_first_marker(input::String, distinct_chars::Int64)
  for i = firstindex(input):(lastindex(input) - distinct_chars - 1)
    unique_chars = Dict()
    for j = i:(i + distinct_chars - 1)
      unique_chars[input[j]] = nothing
    end
    
    if length(unique_chars) == distinct_chars
      return i + distinct_chars - 1
    end
  end
end

day6()
