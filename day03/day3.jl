struct Rucksack
  compartment_a::String
  compartment_b::String
end

struct RucksackGroup
  rucksacks::Array{String}
end

letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

function day3()
  solution1()
  solution2()
end

function solution1()
  rucksacks = getrucksacks()
  total_priority = 0
  
  for rucksack in rucksacks
    total_priority += get_item_priority(rucksack)
  end

  println("Solution 1")
  println(total_priority)
end

function solution2()
  groups = get_rucksack_groups()
  total_priority = 0

  for group in groups
    for item in group.rucksacks[1]
      if occursin(item, group.rucksacks[2]) && occursin(item, group.rucksacks[3])
        total_priority += findfirst(item, letters)
        break
      end
    end
  end

  println("Solution 2")
  println(total_priority)
end

function getinput()::Array{String}
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

function getrucksacks()::Array{Rucksack}
  lines = getinput()
  rucksacks = []

  for line::String in lines
    linelength = length(line)
    compartment_a = line[1:trunc(Int, linelength / 2)]
    compartment_b = line[trunc(Int, linelength / 2 + 1):end]
    push!(rucksacks, Rucksack(compartment_a, compartment_b))
  end

  rucksacks
end

function get_rucksack_groups()::Array{RucksackGroup}
  lines = getinput()
  groups::Array{RucksackGroup} = []
  index = 1

  while index < length(lines)
    group = RucksackGroup(lines[index:(index + 2)])
    push!(groups, group)
    index += 3
  end

  groups
end

function get_item_priority(rucksack::Rucksack)::Int64
  for item in rucksack.compartment_a
    if occursin(item, rucksack.compartment_b)
      return findfirst(item, letters)
    end
  end
end

day3()
