mutable struct Tree
  size::Int64
  isvisible::Bool
  scenic_score::Int64
end

function day8()
  solution1()
  solution2()
end

function solution1()
  lines = getinput()
  trees = traverse_trees(lines)

  visible_trees = filter(x -> x.isvisible, trees)

  println("Solution 1")
  println(length(visible_trees))
end

function solution2()
  lines = getinput()
  trees = traverse_trees(lines)

  max_scenic_score = maximum(x -> x.scenic_score, trees)

  println("Solution 2")
  println(max_scenic_score)
end

function traverse_trees(lines)
  trees = Tree[]

  for i in eachindex(lines)
    for j in eachindex(lines[i])
      tree = Tree(parse(Int64, lines[i][j]), true, 0)

      (visible_top, visible_trees_top) = checktop(tree, lines, i, j)
      (visible_right, visible_trees_right) = checkright(tree, lines, i, j)
      (visible_bottom, visible_trees_bottom) = checkbottom(tree, lines, i, j)
      (visible_left, visible_trees_left) = checkleft(tree, lines, i, j)

      tree.isvisible = visible_top || visible_right || visible_bottom || visible_left
      tree.scenic_score = visible_trees_top * visible_trees_right * visible_trees_bottom * visible_trees_left

      push!(trees, tree)
    end
  end

  trees
end

function checktop(tree, lines, i, j)
  isvisible = true
  visibletrees = 0
  checkindex = i - 1

  while isvisible && checkindex > 0
    visibletrees += 1
    checksize = parse(Int64, lines[checkindex][j])
    if tree.size <= checksize
      isvisible = false
    end
    checkindex -= 1
  end

  (isvisible, visibletrees)
end

function checkright(tree, lines, i, j)
  isvisible = true
  visibletrees = 0
  checkindex = j + 1

  while isvisible && checkindex <= length(lines[i])
    visibletrees += 1
    checksize = parse(Int64, lines[i][checkindex])
    if tree.size <= checksize
      isvisible = false
    end
    checkindex += 1
  end

  (isvisible, visibletrees)
end

function checkbottom(tree, lines, i, j)
  isvisible = true
  visibletrees = 0
  checkindex = i + 1

  while isvisible && checkindex <= length(lines)
    visibletrees += 1
    checksize = parse(Int64, lines[checkindex][j])
    if tree.size <= checksize
      isvisible = false
    end
    checkindex += 1
  end

  (isvisible, visibletrees)
end

function checkleft(tree, lines, i, j)
  isvisible = true
  visibletrees = 0
  checkindex = j - 1

  while isvisible && checkindex > 0
    visibletrees += 1
    checksize = parse(Int64, lines[i][checkindex])
    if tree.size <= checksize
      isvisible = false
    end
    checkindex -= 1
  end

  (isvisible, visibletrees)
end

function getinput()
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

day8()
