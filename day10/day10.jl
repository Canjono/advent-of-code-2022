mutable struct Cycle
  start_x::Int64
  end_x::Int64
end

function day10()
  solution1()
  solution2()
end

function solution1()
  lines = getinput()
  cycles = getcycles(lines)

  signal_strenths = 0
  index = 20

  while index <= 220
    signal_strenths += index * cycles[index].start_x
    index += 40
  end

  println("Solution 1")
  println(signal_strenths)
end

function solution2()
  lines = getinput()
  cycles = getcycles(lines)
  rows = get_draw_rows(cycles)

  println("Solution 2")

  for row in rows
    println(row)
  end
end

function getcycles(lines)::Array{Cycle}
  cycles = []

  for line in lines
    is_first = length(cycles) == 0
    start_x = is_first ? 1 : cycles[end].end_x

    if line == "noop"
      cycle = Cycle(start_x, start_x)
      push!(cycles, cycle)
    else
      cycle = Cycle(start_x, start_x)
      push!(cycles, cycle)

      increase = parse(Int64, split(line, " ")[2])
      cycle = Cycle(start_x, start_x + increase)
      push!(cycles, cycle)
    end
  end

  cycles
end

function get_draw_rows(cycles::Array{Cycle})
  rows = []

  for i in eachindex(cycles)
    is_new_row = i % 40 == 1

    if is_new_row
      push!(rows, "")
    end

    draw_index = (i - 1) % 40
    sprite_index = cycles[i].start_x
    sprite_indices = [sprite_index - 1, sprite_index, sprite_index + 1]

    if findfirst(x -> x == draw_index, sprite_indices) === nothing
      rows[end] *= "."
    else
      rows[end] *= "#"
    end
  end

  rows
end

function getinput()
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

day10()
