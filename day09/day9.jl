mutable struct Knot
  x::Int64
  y::Int64
end

function day9()
  solution1()
  solution2()
end

function solution1()
  lines = getinput()
  knots = []
  amount = 2

  for i in 1:amount
    push!(knots, Knot(0, 0))
  end

  visited_places = moveknots(amount, lines, knots)

  println("Solution 1")
  println(length(visited_places))
end

function solution2()
  lines = getinput()
  knots = []
  amount = 10

  for i in 1:amount
    push!(knots, Knot(0, 0))
  end

  visited_places = moveknots(amount, lines, knots)

  println("Solution 2")
  println(length(visited_places))
end

function moveknots(amount, lines, knots)
  visited_places = Dict()

  for line in lines
    (direction, steps) = split(line, " ")

    for i in 1:parse(Int64, steps)
      movehead(knots[1], direction)

      for j in 2:amount
        head = knots[j - 1]
        current = knots[j]
        distance_x = abs(head.x - current.x)
        distance_y = abs(head.y - current.y)
  
        if distance_x >= 2
          current.x += head.x > current.x ? 1 : -1
  
          if distance_y == 1
            current.y += head.y > current.y ? 1 : -1
          end
        end
  
        if distance_y >= 2
          current.y += head.y > current.y ? 1 : -1
  
          if distance_x == 1
            current.x += head.x > current.x ? 1 : -1
          end
        end
      end

      tail_pos = string(knots[amount].x) * "-" * string(knots[amount].y)
      visited_places[tail_pos] = 1
    end
  end

  visited_places
end

function movehead(head::Knot, direction)
  if direction == "R"
    head.x += 1
  elseif direction == "L"
    head.x -= 1
  elseif direction == "U"
    head.y -= 1
  elseif direction == "D"
    head.y += 1
  end
end

function getinput()
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

day9()