mutable struct Item
  worrylevel::Int128
end

mutable struct Monkey
  id::Int128
  items::Array{Item}
  operation::String
  divisibleby::Int128
  throwtrue::Int128
  throwfalse::Int128
  inspecteditems::Int128
end

function day11()
  solution1()
  solution2()
end

function solution1()
  lines = getinput()
  monkeys = createmonkeys(lines)

  for _ in 1:20
    for monkey in monkeys
      for item in monkey.items
        item.worrylevel = calculate_new_level(monkey.operation, item.worrylevel)
        item.worrylevel = trunc(floor(item.worrylevel / 3))
        passedtest = item.worrylevel % monkey.divisibleby == 0
        throwto_monkey_index = passedtest ? monkey.throwtrue + 1 : monkey.throwfalse + 1
        throwto_monkey = monkeys[throwto_monkey_index]
        push!(throwto_monkey.items, item)
        monkey.inspecteditems += 1
      end
      monkey.items = []
    end
  end

  sorted = sort(map(x -> x.inspecteditems, monkeys), rev=true)

  println("Solution 1")
  println(sorted[1] * sorted[2])
end

function solution2()
  lines = getinput()
  monkeys = createmonkeys(lines)

  total = 1
  for monkey in monkeys
    total *= monkey.divisibleby
  end

  for _ in 1:10000
    for monkey in monkeys
      for item in monkey.items
        item.worrylevel = calculate_new_level(monkey.operation, item.worrylevel)
        item.worrylevel = item.worrylevel % total

        passedtest = item.worrylevel % monkey.divisibleby == 0

        throwto_monkey_index = passedtest ? monkey.throwtrue + 1 : monkey.throwfalse + 1
        throwto_monkey = monkeys[throwto_monkey_index]
        push!(throwto_monkey.items, item)
        monkey.inspecteditems += 1
      end
      monkey.items = []
    end
  end

  sorted = sort(map(x -> x.inspecteditems, monkeys), rev=true)

  println("Solution 2")
  println(sorted[1] * sorted[2])
end

function createmonkeys(lines)
  monkeys = Monkey[]
  monkey = undef
  
  for line in lines
    strippedline = lstrip(line)

    if startswith(strippedline, "Monkey")
      id = parse(Int128, split(line, " ")[2][1])
      monkey = Monkey(id, [], "", 0, 0, 0, 0)
      push!(monkeys, monkey)
    elseif startswith(strippedline, "Starting")
      numbers = split(split(line, ": ")[2], ", ")
      monkey.items = map(x -> Item(parse(Int128, x)), numbers)
    elseif startswith(strippedline, "Operation")
      math = split(line)[5:end]
      monkey.operation = join(math, " ")
    elseif startswith(strippedline, "Test")
      monkey.divisibleby = parse(Int128, split(line)[end])
    elseif startswith(strippedline, "If true")
      monkey.throwtrue = parse(Int128, split(line)[end])
    elseif startswith(strippedline, "If false")
      monkey.throwfalse = parse(Int128, split(line)[end])
    end
  end

  monkeys
end

function calculate_new_level(operation::String, old::Int128)
  (operator, value) = split(operation)

  value = value == "old" ? old : parse(Int128, value)

  return operator == "*" ? old * value : old + value
end

function getinput()
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

day11()