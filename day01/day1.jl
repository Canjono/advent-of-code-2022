function day1()
	solution1()
	solution2()
end

function solution1()
	lines = getinput()

	max_calories = 0
	current_calories = 0

	for line in lines

		if line == ""
			if current_calories > max_calories
				max_calories = current_calories
			end
			current_calories = 0
		else
			current_calories += parse(Int64, line)
		end

	end

	println("Solution 1")
	println(max_calories)
end

function solution2()
	lines = getinput()

	first_calories = 0
	second_calories = 0
	third_calories = 0
	current_calories = 0

	for line in lines

		if line == ""
			if current_calories > first_calories
				third_calories = second_calories
				second_calories = first_calories
				first_calories = current_calories
			elseif current_calories > second_calories
				third_calories = second_calories
				second_calories = current_calories
			elseif current_calories > third_calories
				third_calories = current_calories
			end
			current_calories = 0
		else
			current_calories += parse(Int64, line)
		end

	end

	println("Solution 2")
	println(first_calories + second_calories + third_calories)
end

function getinput()
	file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

day1()
