mutable struct Directory
  name::String
  size::Int64
  dirs::Array{Directory}
  files::Array{String}
end

function day7()
  solution1()
  solution2()
end

function solution1()
  input = getinput()
  dirs = populate_dirs(input)

  matchingdirs = filter(x -> x.size <= 100000, dirs)
  result = 0

  for dir in matchingdirs
    result += dir.size
  end

  println("Solution 1")
  println(result)
end

function solution2()
  input = getinput()
  dirs = populate_dirs(input)
  
  total_space = 70000000 - dirs[1].size
  goal_space = 30000000
  best_diff = total_space
  best_candidate = dirs[1]

  for dir in dirs
    new_space = total_space + dir.size
    new_diff = new_space - goal_space

    if (new_diff > 0 && new_diff < best_diff)
      best_candidate = dir
      best_diff = new_diff
    end
  end


  println("Solution 2")
  println(best_candidate.size)
end

function getinput()
  file = open("./input.txt", "r")
	lines = readlines(file)
	close(file)

	lines
end

function populate_dirs(lines)
  current_dir = Directory("/", 0, [], [])
  root_dir = current_dir
  traversed_dirs = [current_dir]
  dirs = [current_dir]

  for line in lines
    if startswith(line, "\$ cd")
      current_dir = handle_cd!(line, dirs, traversed_dirs, current_dir.dirs)
    elseif startswith(line, "\$ ls")
      # Nothing to do
    elseif startswith(line, "dir ")
      # Nothing to do
    else
      handlefile!(line, traversed_dirs, root_dir, current_dir)
    end
  end

  dirs
end

function handle_cd!(line, dirs, traversed_dirs, child_dirs)
  next_dirname = split(line, " ")[3]

  if next_dirname == ".."
    pop!(traversed_dirs)
    current_dir = traversed_dirs[end]
  elseif next_dirname == "/"
    current_dir = dirs[1]
    traversed_dirs = [current_dir]
  else
    existing_child_dir = filter(x -> x.name == next_dirname, child_dirs)
    current_dir = (length(existing_child_dir) == 0
      ? Directory(next_dirname, 0, [], [])
      : existing_child_dir[1])
    push!(traversed_dirs, current_dir)
    if length(existing_child_dir) == 0
      push!(dirs, current_dir)
    end
  end

  return current_dir
end

function handlefile!(line, traversed_dirs, root_dir, current_dir)
  (size, filename) = split(line, " ")
  already_handled = length(filter(x -> x == filename, current_dir.files)) > 0

  if already_handled return end

  current_dir.size += parse(Int64, size)
  push!(current_dir.files, filename)

  if current_dir == root_dir return end

  for parent in traversed_dirs[1:(length(traversed_dirs) - 1)]
    parent.size += parse(Int64, size)
  end
end

day7()
